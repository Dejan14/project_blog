    <footer>
        <div class="container text-center">
            <h4>Copyright 2016 | Dejan Milić</h4>
        </div>
    </footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery-1.12.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
<?php if(isset($db)) { $db->close_connection(); } ?>
