    <div class="box">
        <h3>O NAMA</h3>
        <p>"Design by DANI" brend je namenjen modernim damama koje su u pokretu tokom celog dana, ali i za one koje žele da svojom posebnošću zablistaju na nekom događaju ili u večernjem izlasku.</p>
        <img src="img/dani-01.jpg" class="img-responsive" alt="Design-by-DANI-01">
        <p>Iza mladog brenda "Design by DANI" stoji mlada modna dizajnerka Danijela Milić iz Beograda. Mlada umetnica kombinuje jednostavne krojeve sa prijatnim materijalima.</p>
        <img src="img/dani-02.jpg" class="img-responsive" alt="Design-by-DANI-02">
        <p>Kreacije odišu čistim linijama, prelepim tkaninama i neizbežnim detaljima. Svaka haljina je ručni rad.</p>
    </div>
