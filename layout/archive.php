    <div class="box">
        <h3>ARHIVA</h3>
        <?php
        $prev_year = null;
        $prev_month = null;
        setlocale(LC_TIME, 'sr_RS@latin');
        $posts = Posts::find_all();
        foreach($posts as $post) {
            $new_year = strftime("%Y", strtotime($post->datepost));
            $new_month = strftime("%B", strtotime($post->datepost));
            if($prev_year != $new_year) {
                echo "<h4>". $new_year ."</h4>";
            }
            if($prev_month != $new_month) {
                echo "<h5><b>". $new_month ."</b></h5>";
            }
            echo "<h5><i><a href=\"post.php?id=" . urlencode($post->id) ."\">". htmlentities($post->caption) ."</a></i></h5>";
            $prev_year = $new_year;
            $prev_month = $new_month;
        }
        ?>
    </div>
