<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DANI | Blog</title>
    <link rel="icon" href="img/girls-black-dress.png">
    <link href="bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <section id="navbar">
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/blog/">DANI | Blog</a>
                </div> <!-- End Navbar Header -->
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/blog/">HOME</a></li>

                        <?php $categories = Categories::find_all();
                        foreach($categories as $category) { ?>
                            <li><a href="category.php?id=<?php echo urlencode($category->id); ?>"><?php echo strtoupper(htmlentities($category->category_name)); ?></a></li>
                        <?php } ?>

                    </ul>
                </div>
            </div>
        </div>
    </section> <!-- End Navbar -->
