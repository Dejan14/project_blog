<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>DANI | Blog</title>
    <link rel="icon" href="../img/girls-black-dress.png">
    <link href="../bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="../css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <section id="navbar">
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/blog/">DANI | Blog</a>
                </div> <!-- End Navbar Header -->
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">

                        <?php $user = User::find_by_id($_SESSION['user_id']); ?>

                        <li><a href="/blog/admin/">User | <?php echo strtoupper($user->username); ?></a></li>
                        <li><a href="new_user.php">USERS</a></li>
                        <li><a href="new_post.php">NEW POST</a></li>
                        <li><a href="logout.php">LOGOUT</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section> <!-- End Navbar -->
