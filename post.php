<?php require_once("include/initialize.php"); ?>
<?php
    if(empty($_GET['id'])) {
        $session->message("No post ID was provided.");
        redirect_to('index.php');
    }

    $post = Posts::find_by_id($_GET['id']);

    if(!$post) {
        $session->message("The post could not be located.");
        redirect_to('index.php');
    }

    if(isset($_POST['submit'])) {
        $author = trim($_POST['author']);
        $body = trim($_POST['body']);

        if($author == "" || $body == "") {
            $message = "Please fill out all fields bellow.";
        } else {
            $new_comment = Comment::make($post->id, $author, $body);
            if($new_comment && $new_comment->create()) {
                $to = "Dejan Milić <dekimilic14@gmail.com>";
                $subject = "Novi komentar za post " .$post->caption;
                $komentar = "{$author}: {$body}";
                mail($to, $subject, $komentar);
                redirect_to("post.php?id={$post->id}");
            } else {
                $message = "Comment was not sent. Please try again.";
            }
        }
	} else {
		$author = "";
		$body = "";
	}

    $comments = $post->comments();
?>
<?php include_layout_template('header.php'); ?>

    <section id="main">
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-9">
                    <div class="post">
                        <div class="page-header">
                            <h1><?php echo htmlentities($post->caption); ?></h1>
                            <h4><?php echo date_to_text($post->datepost); ?></h4>
                        </div>
                        <div>
                            <p><?php echo htmlentities($post->textpost); ?></p>
                            <img src="<?php echo htmlentities($post->image_path()); ?>" class="img-responsive" alt="<?php echo htmlentities($post->caption); ?>">
                        </div>
                    </div>
                    <div id="comment" class="comments">
                        <h3>KOMENTARI</h3>

                        <?php foreach($comments as $comment): ?>

                        <div class="comment">
                            <div class="author">
                                <b><?php echo htmlentities($comment->author); ?>:</b>
                            </div>
                            <div class="body">
				                <?php echo htmlentities($comment->body); ?>
                            </div>
                            <div class="meta-info">
                                <?php echo datetime_to_text($comment->created); ?>
                            </div>
                        </div>

                        <?php endforeach; ?>
                        <?php if(empty($comments)) { echo "Nema komentara."; } ?>

                    </div>
                    <div class="col-sm-8 col-sm-offset-2 comment-form">
                        <h3>NOVI KOMENTAR</h3>

                        <?php echo output_message($message); ?>

                        <form action="post.php?id=<?php echo urlencode($post->id); ?>#comment" class="form-horizontal" method="POST">
                            <div class="form-group">
                                <label for="author" class="sr-only">Ime:</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="author" id="author" value="<?php echo $author; ?>" placeholder="Ime">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="body" class="sr-only">Komentar:</label>
                                <div class="col-sm-12">
                                    <textarea type="text" class="form-control" name="body" id="body" rows="5" placeholder="Komentar"><?php echo $body; ?></textarea>
                                </div>
                            </div>
                            <button type="submit" name="submit" class="btn btn-danger btn-block">POŠALJI KOMENTAR</button>
                        </form>

                    </div>
                    <div class="col-sm-3 col-sm-offset-3 col-xs-6 text-right"> <!-- Previous Post -->

                    <?php
                        $id_previous = $_GET['id'] - 1;
                        $post_previous = Posts::find_by_id($id_previous);
                        if ($post_previous) {
                    ?>

                        <a class="thumbnail" href="post.php?id=<?php echo urlencode($post_previous->id); ?>" title="Previous">
                            <img src="<?php echo htmlentities($post_previous->image_path()); ?>" class="img-responsive photo-real" alt="<?php echo htmlentities($post_previous->caption); ?>">
                            <img src="<?php echo htmlentities($post_previous->image_glass_path()); ?>" class="img-responsive photo-glass">
                            <div class="caption">
                                <h4><span class="glyphicon glyphicon-hand-left"></span><br><?php echo htmlentities($post_previous->caption); ?></h4>
                            </div>
                        </a>

                    <?php } ?>

                    </div>
                    <div class="col-sm-3 col-xs-6 text-left"> <!-- Next Post -->

                    <?php
                        $id_next = $_GET['id'] + 1;
                        $post_next = Posts::find_by_id($id_next);
                        if ($post_next) {
                    ?>

                        <a class="thumbnail" href="post.php?id=<?php echo urlencode($post_next->id); ?>" title="Next">
                            <img src="<?php echo htmlentities($post_next->image_path()); ?>" class="img-responsive photo-real" alt="<?php echo htmlentities($post_next->caption); ?>">
                            <img src="<?php echo htmlentities($post_next->image_glass_path()); ?>" class="img-responsive photo-glass">
                            <div class="caption">
                                <h4><span class="glyphicon glyphicon-hand-right"></span><br><?php echo htmlentities($post_next->caption); ?></h4>
                            </div>
                        </a>

                    <?php } ?>

                    </div>
                </div>
                <div class="clearfix visible-xs-block"></div>
                <div class="col-sm-3">

                <?php include_layout_template('about_us.php'); ?>
                <?php include_layout_template('archive.php'); ?>

                </div>
            </div>
        </div>
    </section>

<?php include_layout_template('footer.php'); ?>
