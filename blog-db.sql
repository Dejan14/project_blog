-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: dani_blog
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog_category`
--

DROP TABLE IF EXISTS `blog_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_category`
--

LOCK TABLES `blog_category` WRITE;
/*!40000 ALTER TABLE `blog_category` DISABLE KEYS */;
INSERT INTO `blog_category` VALUES (1,'celebrities'),(2,'lifestyle'),(3,'collections');
/*!40000 ALTER TABLE `blog_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_comment`
--

DROP TABLE IF EXISTS `blog_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_post_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `body` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_comment`
--

LOCK TABLES `blog_comment` WRITE;
/*!40000 ALTER TABLE `blog_comment` DISABLE KEYS */;
INSERT INTO `blog_comment` VALUES (1,10,'2016-04-11 12:31:59','Milica','Predivna haljina.'),(2,10,'2016-04-11 12:33:01','Una','Sjajan kroj!'),(3,9,'2016-04-11 12:54:22','Ana','Baš lepa haljina.'),(4,9,'2016-04-11 13:02:25','Maria','Nice dress.'),(5,10,'2016-04-11 13:24:03','Nevena','Divna haljina !'),(6,7,'2016-04-11 13:30:44','Sara','Odlična haljina.'),(7,7,'2016-04-11 13:34:53','Milica','Divna!'),(8,1,'2016-04-11 13:36:05','Ina','Lepa slika.'),(9,1,'2016-04-11 13:38:08','Sara','Very nice dress.');
/*!40000 ALTER TABLE `blog_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_post`
--

DROP TABLE IF EXISTS `blog_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(90) DEFAULT NULL,
  `textpost` mediumtext,
  `datepost` date DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `filename_glass` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_post`
--

LOCK TABLES `blog_post` WRITE;
/*!40000 ALTER TABLE `blog_post` DISABLE KEYS */;
INSERT INTO `blog_post` VALUES (1,'Obucite se cice','Saradnja sa blogom \"Jedan frajer i bidermajer\".','2015-09-17','biedermeier.jpg','biedermeier-glass.jpg',2),(2,'Story party','Roksanda Babić u našoj unikatnoj haljini.','2015-10-01','story.jpg','story-glass.jpg',1),(3,'Dama','Saradnja sa blogom \"Niksis\".','2015-10-29','niksis4.jpg','niksis4-glass.jpg',2),(4,'Pastel 2015','Unikatne haljine u pastelnim bojama. Materijal brokat, loran i pamuk u kombinaciji sa biserima.','2015-11-05','pastel.jpg','pastel-glass.jpg',3),(5,'Zalazak sunca','\"Design by DANI\" unikatna haljina na blogu \"Niksis\".','2015-11-13','sunset.jpg','sunset-glass.jpg',2),(6,'Zlatna jesen','Saradnja sa blogom \"Sugar Journal by Rendzi\".','2015-11-17','golden-autumn.jpg','golden-autumn-glass.jpg',2),(7,'Hello awards','Marija Veljković  u \"Design by DANI\" haljini.','2015-12-10','hello-awards.jpg','hello-awards-glass.jpg',1),(8,'Triangles 2016','Unikatne haljine u tamnim bojama. Materijal brokat i loran u kombinaciji sa kristalima.','2016-01-15','triangles.jpg','triangles-glass.jpg',3),(9,'Kristina Kuzmanovska','Pevačica Kristina Kuzmanovska u \"Design by DANI\" unikatnoj haljini.','2016-02-05','kristina.jpg','kristina-glass.jpg',1),(10,'Night 2016','Unikatne haljine za večernje izlaske. Materijal brokat i loran u kombinaciji sa kristalima i šljokicama.','2016-02-15','night.jpg','night-glass.jpg',3);
/*!40000 ALTER TABLE `blog_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_user`
--

DROP TABLE IF EXISTS `blog_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_user`
--

LOCK TABLES `blog_user` WRITE;
/*!40000 ALTER TABLE `blog_user` DISABLE KEYS */;
INSERT INTO `blog_user` VALUES (1,'dejan','$2y$10$AjOi68FripDKQ8//Umuj5uBtYyYiF9hz26.fO0CkP2YCjBkMRV/M2'),(4,'daca','$2y$10$er4RFWiHyc5VTiICsY2MFeEYUnJ5M1c4PqtbdfzCvB2tgyboHbFWe');
/*!40000 ALTER TABLE `blog_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-16 16:53:44
