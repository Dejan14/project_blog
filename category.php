<?php require_once("include/initialize.php"); ?>
<?php
    if(empty($_GET['id'])) {
        $session->message("No category ID was provided.");
        redirect_to('index.php');
    }

    $posts = Posts::find_all_posts_by_category($_GET['id']);
    $category = Categories::find_by_id($_GET['id']);

    if(!$posts) {
        $session->message("The posts could not be located.");
        redirect_to('index.php');
    }
?>
<?php include_layout_template('header.php'); ?>

    <section id="main">
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-9">
                    <h1><?php echo strtoupper(htmlentities($category->category_name)); ?></h1>

                    <?php foreach($posts as $post) { ?>

                    <div class="category">
                        <div class="page-header">
                            <h2><?php echo htmlentities($post->caption); ?></h2>
                            <h4><?php echo date_to_text($post->datepost); ?></h4>
                        </div>
                        <div>
                            <p><?php echo htmlentities($post->textpost); ?></p>
                            <img src="<?php echo htmlentities($post->image_path()); ?>" class="img-responsive" alt="<?php echo htmlentities($post->caption); ?>">
                        </div>
                    </div>

                    <?php } ?>

                </div>
                <div class="clearfix visible-xs-block"></div>
                <div class="col-sm-3">

                <?php include_layout_template('about_us.php'); ?>
                <?php include_layout_template('archive.php'); ?>

                </div>
            </div>
        </div>
    </section>

<?php include_layout_template('footer.php'); ?>
