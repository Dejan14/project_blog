<?php require_once("include/initialize.php"); ?>
<?php
    if($session->is_logged_in()) { redirect_to("admin/"); }

    if(isset($_POST['submit'])) {

        $username = trim($_POST['username']);
        $password = trim($_POST['password']);

        $user = User::find_by_username($username);

        if($user) {
            if(password_verify($password, $user->password)) {
                $session->login($user);
                $session->message("Hello {$username}.");
                redirect_to("admin/");
            } else {
                $message = "Username/password combination incorrect.";
            }
        } else {
            $message = "Username/password combination incorrect.";
        }
    } else {
        $username = "";
        $password = "";
    }
?>
<?php include_layout_template('header.php'); ?>

    <section id="main">
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-9">

                    <?php $posts = Posts::find_all();
                    foreach($posts as $post) { ?>

                        <div class="col-sm-4 home-post">
                            <a class="thumbnail" href="post.php?id=<?php echo urlencode($post->id); ?>">
                                <img src="<?php echo htmlentities($post->image_path()); ?>" class="img-responsive photo-real" alt="<?php echo htmlentities($post->caption); ?>">
                                <img src="<?php echo htmlentities($post->image_glass_path()); ?>" class="img-responsive photo-glass">
                                <div class="caption caption-date">
                                    <p><?php echo date_to_text($post->datepost); ?></p>
                                </div>
                                <div class="caption">
                                    <h2><?php echo htmlentities($post->caption); ?></h2>
                                </div>
                            </a>
                        </div>

                    <?php } ?>

                </div>
                <div class="col-sm-3">

                <?php echo output_message($message); ?>

                    <div class="box box-first">
                        <h3>LOGIN</h3>
                        <form action="/blog/" class="form-horizontal" method="POST">
                            <div class="form-group">
                                <label for="username" class="sr-only">Username:</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="username" id="username" maxlength="45" value="<?php echo htmlentities($username); ?>" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="sr-only">Password:</label>
                                <div class="col-sm-12">
                                    <input type="password" class="form-control" name="password" id="password" maxlength="45" value="<?php echo htmlentities($password); ?>" placeholder="Password">
                                </div>
                            </div>
                            <button type="submit" name="submit" class="btn btn-danger btn-block">LOGIN</button>
                        </form>
                    </div>

                    <?php include_layout_template('about_us.php'); ?>
                    <?php include_layout_template('archive.php'); ?>

                </div>
            </div>
        </div>
    </section>

<?php include_layout_template('footer.php'); ?>
