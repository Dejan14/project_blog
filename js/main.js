/*global $, jQuery*/
(function ($) {
    "use strict"; // Start of use strict

    $(document).ready(function () {
        $("img.photo-glass").hover(
            function () {
                $(this).stop().animate({"opacity": "0"}, 500);
            },
            function () {
                $(this).stop().animate({"opacity": "1"}, 500);
            }
        );
    });

}(jQuery)); // End of use strict
