<?php
require_once(LIB_PATH.DS."db.php");

class User extends DatabaseObject {

	protected static $table_name="blog_user";
    protected static $db_fields = array('id', 'username', 'password');
    public $id;
	public $username;
	public $password;

    public static function find_by_username($username) {
        global $db;

        $sql  = "SELECT * FROM " . self::$table_name;
        $sql .= " WHERE username = '{$username}'";
        $sql .= " LIMIT 1";
        $result_array = self::find_by_sql($sql);
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public function add_user() {
        global $db;

        $username = $db->escape_value($this->username);
        $password = $db->escape_value($this->password);

        $password_hash = password_hash($password, PASSWORD_DEFAULT);

        $sql = "INSERT INTO blog_user (";
        $sql .= "username, password";
        $sql .= ") VALUES ('";
		$sql .= $username ."', '". $password_hash;
        $sql .= "')";
        if($db->query($sql)) {
            $this->id = $db->insert_id();
            return true;
        } else {
            return false;
        }
	}
}

?>
