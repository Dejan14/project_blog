<?php

    function redirect_to( $location = NULL ) {
        if ($location != NULL) {
            header("Location: {$location}");
            exit;
        }
    }

    function output_message($message="") {
        if (!empty($message)) {
            return "<p class=\"message\">{$message}</p>";
        } else {
            return "";
        }
    }

    function __autoload($class_name) {
        $class_name = strtolower($class_name);
        $path = "include/{$class_name}.php";
        if(file_exists($path)) {
            require_once($path);
        } else {
            die("The file {$class_name}.php could not be found.");
        }
    }

    function include_layout_template($template="") {
        include(SITE_ROOT.DS.'layout'.DS.$template);
    }

    function datetime_to_text($datetime="") {
        setlocale(LC_TIME, 'sr_RS@latin');
        $unixdatetime = strtotime($datetime);
        return strftime("%d. %B %Y, %H:%M", $unixdatetime);
    }

    function date_to_text($date) {
        $timestamp = strftime("%m/%d/%Y", strtotime($date));
        return $timestamp;
    }

?>
