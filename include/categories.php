<?php
require_once(LIB_PATH.DS."db.php");

class Categories extends DatabaseObject {

	protected static $table_name="blog_category";
    protected static $db_fields = array('id', 'category_name');
    public $id;
	public $category_name;

}
