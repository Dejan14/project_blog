<?php
require_once(LIB_PATH.DS.'db.php');

class Comment extends DatabaseObject {

    protected static $table_name="blog_comment";
    protected static $db_fields=array('id', 'blog_post_id', 'created', 'author', 'body');

    public $id;
    public $blog_post_id;
    public $created;
    public $author;
    public $body;

	public static function make($post_id, $author="Anonymous", $body="") {
        if(!empty($post_id) && !empty($author) && !empty($body)) {
			$comment = new Comment();
            $comment->blog_post_id = (int)$post_id;
            $comment->created = strftime("%Y-%m-%d %H:%M:%S", time());
            $comment->author = $author;
            $comment->body = $body;
            return $comment;
		} else {
			return false;
		}
	}

	public static function find_comments_on($post_id=0) {
        global $db;

        $sql = "SELECT * FROM " . self::$table_name;
        $sql .= " WHERE blog_post_id=" .$db->escape_value($post_id);
        $sql .= " ORDER BY created ASC";
        return self::find_by_sql($sql);
	}
}

?>
