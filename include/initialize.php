<?php

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

defined('SITE_ROOT') ? null :
	define('SITE_ROOT', $_SERVER['DOCUMENT_ROOT'].DS.'blog');

defined('LIB_PATH') ? null : define('LIB_PATH', SITE_ROOT.DS.'include');

// load connection file
require_once(LIB_PATH.DS.'config.php');

// load basic functions
require_once(LIB_PATH.DS.'functions.php');

// load core objects
require_once(LIB_PATH.DS.'session.php');
require_once(LIB_PATH.DS.'db.php');
require_once(LIB_PATH.DS.'db_object.php');

// load database-related classes
require_once(LIB_PATH.DS.'user.php');
require_once(LIB_PATH.DS.'categories.php');
require_once(LIB_PATH.DS.'posts.php');
require_once(LIB_PATH.DS.'comment.php');

?>
