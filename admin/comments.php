<?php
require_once("../include/initialize.php");

    if (!$session->is_logged_in()) { redirect_to("../"); }

    if(empty($_GET['id'])) {
        $session->message("No post ID was provided.");
        redirect_to('index.php');
    }

    $post = Posts::find_by_id($_GET['id']);

    if(!$post) {
        $session->message("The post could not be located.");
        redirect_to('index.php');
    }

	$comments = $post->comments();
?>
<?php include_layout_template('header_admin.php'); ?>

    <section id="admin">
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-12">
                    <h1>Komentari za post <?php echo htmlentities($post->caption); ?></h1>

                    <?php echo output_message($message); ?>

                    <div class="comments">

                    <?php foreach($comments as $comment): ?>

                        <div class="comment">
                            <div class="author">
                                <b><?php echo htmlentities($comment->author); ?>:</b>
                            </div>
                            <div class="body">
                                <?php echo htmlentities($comment->body); ?>
                            </div>
                            <div class="meta-info">
                                <?php echo datetime_to_text($comment->created); ?>
                            </div>
                            <div class="actions">
                                <a href="delete_comment.php?id=<?php echo urlencode($comment->id); ?>" onclick="return confirm('Are you sure?');">Delete Comment</a>
                            </div>
                        </div>

                    <?php endforeach; ?>
                    <?php if(empty($comments)) { echo "Nema komentara."; } ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include_layout_template('footer_admin.php'); ?>
