<?php
require_once('../include/initialize.php');

    if (!$session->is_logged_in()) { redirect_to("../"); }

	if(isset($_POST['submit'])) {
        if($_POST['username'] == "" || $_POST['password'] == "") {
            $message = "Please fill out all fields bellow.";
        } else {
            $user = new User();
            $user->username = $_POST['username'];
            $user->password = $_POST['password'];
            if($user->add_user()) {
                $message = "The user was added successfully.";
            } else {
                $message = "Failed. Try again.";
            }
        }
	}

?>
<?php include_layout_template('header_admin.php'); ?>

    <section id="admin">
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-3 col-sm-offset-3 users">
                    <h1>KORISNICI</h1>

                    <?php $users = User::find_all(); ?>

                    <ul>

                        <?php
                        foreach($users as $user) {
                            echo "<li>". strtoupper(htmlentities($user->username)) ."</li>";
                        }
                        ?>

                    </ul>
                </div>
                <div class="col-sm-3">
                    <h1>NEW USER</h1>

                    <?php echo output_message($message); ?>

                    <form action="new_user.php" class="form-horizontal" method="POST">
                        <div class="form-group">
                            <label for="username" class="sr-only">Username:</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="username" id="username" maxlength="45" value="" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="sr-only">Password:</label>
                            <div class="col-sm-12">
                                <input type="password" class="form-control" name="password" id="password" maxlength="45" value="" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-danger btn-block" name="submit">ADD USER</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php include_layout_template('footer_admin.php'); ?>
