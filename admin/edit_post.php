<?php
require_once("../include/initialize.php");

    if (!$session->is_logged_in()) { redirect_to("../"); }

    if(empty($_GET['id'])) {
        $session->message("No post ID was provided.");
        redirect_to('index.php');
    }

    $post = Posts::find_by_id($_GET['id']);

    if(!$post) {
        $session->message("The post could not be located.");
        redirect_to('index.php');
    }

    if(isset($_POST['submit'])) {
        if($_POST['caption'] == "" || $_POST['textpost'] == "" || $_POST['datepost'] == "" || $_POST['category_id'] == "") {
            $message = "Please fill out all fields bellow.";
        } else {
            $post->caption = $_POST['caption'];
            $post->textpost = $_POST['textpost'];
            $post->datepost = $_POST['datepost'];
            $post->category_id = $_POST['category_id'];
            if($post && $post->update()) {
                $session->message("Post {$post->caption} was updated.");
                redirect_to('index.php');
            } else {
                $message = "The post was not updated.";
            }
        }
	}
?>
<?php include_layout_template('header_admin.php'); ?>

    <section id="admin">
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-12">
                    <h1>EDIT POST</h1>

                    <?php echo output_message($message); ?>

                    <form action="edit_post.php?id=<?php echo urlencode($post->id); ?>" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="caption" class="col-sm-3 control-label">Caption:</label>
                            <div class="col-sm-6">
                                <input type="text" id="caption" name="caption" class="form-control" value="<?php echo htmlentities($post->caption); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date" class="col-sm-3 control-label">Date:</label>
                            <div class="col-sm-6">
                                <input type="date" id="datepost" name="datepost" class="form-control" value="<?php echo $post->datepost; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="textpost" class="col-sm-3 control-label">Text:</label>
                            <div class="col-sm-6">
                                <textarea id="textpost" name="textpost" class="form-control" cols="10" rows="10"><?php echo htmlentities($post->textpost); ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="category_id" class="col-sm-3 control-label">Category:</label>
                            <div class="col-sm-6">
                                <select id="category_id" name="category_id" class="form-control">
                                    <option value="1" <?php if($post->category_id == 1) {echo "selected='selected'";} ?>>Celebrities</option>
                                    <option value="2" <?php if($post->category_id == 2) {echo "selected='selected'";} ?>>Lifestyle</option>
                                    <option value="3" <?php if($post->category_id == 3) {echo "selected='selected'";} ?>>Collections</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-danger btn-block" name="submit">EDIT POST</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php include_layout_template('footer_admin.php'); ?>
