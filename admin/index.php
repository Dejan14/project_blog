<?php require_once('../include/initialize.php'); ?>
<?php if (!$session->is_logged_in()) { redirect_to("../"); } ?>
<?php include_layout_template('header_admin.php'); ?>

    <section id="admin">
        <div class="container-fluid text-center">
            <div class="row">

                    <?php echo output_message($message); ?>
                    <?php $posts = Posts::find_all();
                    foreach($posts as $post) { ?>

                        <div class="col-sm-3 home-post">
                            <div class="thumbnail">
                                <img src="../<?php echo htmlentities($post->image_path()); ?>" class="img-responsive" alt="<?php echo htmlentities($post->caption); ?>">
                                <div class="caption caption-date">
                                    <p><?php echo date_to_text($post->datepost); ?></p>
                                </div>
                                <div class="caption">
                                    <h3><?php echo htmlentities($post->caption); ?></h3>
                                    <p><?php echo htmlentities($post->textpost); ?></p>
                                    <a class="btn btn-danger btn-xs" href="comments.php?id=<?php echo urlencode($post->id); ?>" role="button">COMMENTS (<?php echo count($post->comments()); ?>)</a>
                                    <a class="btn btn-danger btn-xs" href="edit_post.php?id=<?php echo urlencode($post->id); ?>" role="button">EDIT</a>
                                    <a class="btn btn-danger btn-xs" href="delete_post.php?id=<?php echo urlencode($post->id); ?>" role="button">DELETE</a>
                                </div>
                            </div>
                        </div>

                    <?php } ?>

            </div>
        </div>
    </section>

<?php include_layout_template('footer_admin.php'); ?>
