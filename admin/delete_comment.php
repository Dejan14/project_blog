<?php
require_once("../include/initialize.php");

    if (!$session->is_logged_in()) { redirect_to("../"); }

    if(empty($_GET['id'])) {
        $session->message("No comment ID was provided.");
        redirect_to('index.php');
    }

    $comment = Comment::find_by_id($_GET['id']);

    if($comment && $comment->delete()) {
        $session->message("The comment was deleted.");
        redirect_to("comments.php?id={$comment->post_test_id}");
    } else {
        $session->message("The comment could not be deleted.");
        redirect_to('index.php');
    }

?>
<?php if(isset($db)) { $db->close_connection(); } ?>
