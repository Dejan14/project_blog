<?php
require_once('../include/initialize.php');

    if (!$session->is_logged_in()) { redirect_to("../"); }

	$max_file_size = 1048576;

	if(isset($_POST['submit'])) {
        if($_POST['caption'] == "" || $_POST['textpost'] == "" || $_POST['datepost'] == "" || $_POST['category_id'] == "") {
            $message = "Please fill out all fields bellow.";
        } else {
            $post = new Posts();
            $post->caption = $_POST['caption'];
            $post->textpost = $_POST['textpost'];
            $post->datepost = $_POST['datepost'];
            $post->category_id = $_POST['category_id'];
            $post->attach_file($_FILES['file_upload']);
            if($post->save()) {
                $session->message("Post {$post->caption} was added.");
                redirect_to('index.php');
            } else {
                $message = join("<br />", $post->errors);
            }
        }
	}

?>
<?php include_layout_template('header_admin.php'); ?>

    <section id="admin">
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-12">
                    <h1>NEW POST</h1>

                    <?php echo output_message($message); ?>

                    <form action="new_post.php" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="caption" class="col-sm-3 control-label">Caption:</label>
                            <div class="col-sm-6">
                                <input type="text" id="caption" name="caption" class="form-control" value="<?php if(isset($_POST['submit'])) {echo $_POST['caption'];} ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date" class="col-sm-3 control-label">Date:</label>
                            <div class="col-sm-6">
                                <input type="date" id="datepost" name="datepost" class="form-control" value="<?php if(isset($_POST['submit'])) {echo $_POST['datepost'];} ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="textpost" class="col-sm-3 control-label">Text:</label>
                            <div class="col-sm-6">
                                <textarea id="textpost" name="textpost" class="form-control" cols="10" rows="10"><?php if(isset($_POST['submit'])) {echo $_POST['textpost'];} ?></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $max_file_size; ?>">
                        <div class="form-group">
                            <label for="file_upload" class="col-sm-3 control-label">Image:</label>
                            <div class="col-sm-6">
                                <input type="file" id="file_upload" name="file_upload">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="category_id" class="col-sm-3 control-label">Category:</label>
                            <div class="col-sm-6">
                                <select id="category_id" name="category_id" class="form-control">
                                    <option value="1">Celebrities</option>
                                    <option value="2">Lifestyle</option>
                                    <option value="3">Collections</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-danger btn-block" name="submit">ADD POST</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php include_layout_template('footer_admin.php'); ?>
