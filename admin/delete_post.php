<?php
require_once("../include/initialize.php");

    if (!$session->is_logged_in()) { redirect_to("../"); }

    if(empty($_GET['id'])) {
        $session->message("No post ID was provided.");
        redirect_to('index.php');
    }

    $post = Posts::find_by_id($_GET['id']);

    if(!$post) {
        $session->message("The post could not be located.");
        redirect_to('index.php');
    }
?>
<?php include_layout_template('header_admin.php'); ?>

    <section id="admin">
        <div class="container text-center">
            <div class="row">

            <?php
                if(isset($_POST['del'])) {
                    if($post && $post->destroy()) {
                        $session->message("Post {$post->caption} was deleted.");
                        redirect_to('index.php');
                    } else {
                        $session->message("The post could not be deleted.");
                    }
                } elseif(isset($_POST['quit'])) {
                    redirect_to('index.php');
                } else { ?>

                <div class="col-sm-6 col-sm-offset-1">
                    <h1>DELETE POST</h1>

                    <?php echo output_message($message); ?>

                    <h2>Da li sugurno želite da obrišete post<br><b><?php echo htmlentities($post->caption); ?></b>?</h2>
                    <form action="delete_post.php?id=<?php echo urlencode($post->id); ?>" method="POST">
                        <input class="btn btn-danger" type="submit" name="del" value="YES">
                        <input class="btn btn-danger" type="submit" name="quit" value="NO">
                    </form>
                </div>
                <br>
                <div class="col-sm-4">
                    <img src="../<?php echo htmlentities($post->image_path()); ?>" class="img-responsive">
                </div>

            <?php } ?>

            </div>
        </div>
    </section>

<?php include_layout_template('footer_admin.php'); ?>
